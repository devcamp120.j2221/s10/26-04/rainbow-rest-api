package com.devcamp.rainbowrestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// Khai báo Class này là 1 controller
@RestController
// Khai báo CORS
@CrossOrigin(value = "*", maxAge = -1)
// Prefix API
@RequestMapping("/")
public class RainbowController {
    @GetMapping("/rainbow")
    public ArrayList<String> getRainbowColor() {
        ArrayList<String> rainbowColors = new ArrayList<>();

        rainbowColors.add("red");
        rainbowColors.add("orange");
        rainbowColors.add("yellow");
        rainbowColors.add("green");
        rainbowColors.add("blue");
        rainbowColors.add("indigo");
        rainbowColors.add("violet");
        
        return rainbowColors;
    }
}
